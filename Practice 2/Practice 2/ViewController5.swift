//
//  ViewController5.swift
//  Practice 2
//
//  Created by Егор Бадмаев on 03.04.2024.
//

import UIKit

final class ViewController5: UIViewController {
    
    @IBOutlet weak var textField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UNUserNotificationCenter
            .current()
            .requestAuthorization(options: [.alert, .badge, .sound]) { success, error in
                if let error {
                    assertionFailure("Ошибка при запросе разрешения: \(error.localizedDescription)")
                }
            }
    }
    
    func showToast(message: String, duration: TimeInterval = 1.0) {
        let label = UILabel()
        label.backgroundColor = UIColor.black.withAlphaComponent(0.85)
        label.font = UIFont.systemFont(ofSize: 12.0)
        label.layer.cornerRadius = 10
        label.textAlignment = .center
        label.clipsToBounds = true
        label.textColor = .white
        label.numberOfLines = 0
        label.text = message
        
        let width = label.intrinsicContentSize.width + 48
        let height = label.intrinsicContentSize.height + 32
        let tabBarHeight = tabBarController?.tabBar.frame.height ?? 0
        label.frame = CGRect(
            x: view.frame.size.width / 2 - width / 2,
            y: view.frame.size.height - height - tabBarHeight - 20,
            width: width,
            height: height
        )
        view.addSubview(label)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + duration) {
            UIView.animate(withDuration: 1.0, delay: 0.1, options: .curveEaseOut, animations: {
                label.alpha = 0.0
            }, completion: { _ in
                label.removeFromSuperview()
            })
        }
    }
    
    @IBAction func openToast(_ sender: Any) {
        showToast(
            message: "СТУДЕНТ №2 ГРУППА 10 Количество символов - \(textField.text?.count ?? 0)"
        )
    }
    
    @IBAction func onClickSendNotification(_ sender: Any) {
        let content = UNMutableNotificationContent()
        content.title = "MIREA"
        content.body = "Congratulations!"
        content.sound = UNNotificationSound.default
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 2, repeats: false)
        let request = UNNotificationRequest(identifier: "MireaNotification", content: content, trigger: trigger)
        UNUserNotificationCenter.current().add(request)
    }
}
