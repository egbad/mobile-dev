# Практическая работа №2

| Navigation 1 | Navigation 2 | Web |
| ------------- |:-------------|:-------------:|
| ![Navigation 1](Screens/1.png) | ![Navigation 2](Screens/2.png) | ![Web](Screens/3.png) |
| Web In App | Web Browser | Dialogs 1 |
| ![Web In App](Screens/4.png) | ![Web Browser](Screens/5.png) | ![Dialogs 1](Screens/6.png) |
| Dialogs 2 | Notification 1 | Notification 2 |
| ![Dialogs 2](Screens/7.png) | ![Notification 1](Screens/8.png) | ![Notification 2](Screens/9.png) |
| Notification 3 |  |  |
| ![Notification 3](Screens/10.png) |  |  |
