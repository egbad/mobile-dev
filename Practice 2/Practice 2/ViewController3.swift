//
//  ViewController3.swift
//  Practice 2
//
//  Created by Егор Бадмаев on 17.03.2024.
//

import UIKit

final class ViewController3: UIViewController {
    
    @IBOutlet weak var progress: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        progress.startAnimating()
    }
    
    @IBAction func timePickerAction(_ sender: Any) {
        guard let timePicker = sender as? UIDatePicker else { return }
        print(timePicker.date)
    }
    
    @IBAction func datePickerAction(_ sender: Any) {
        guard let datePicker = sender as? UIDatePicker else { return }
        print(datePicker.date)
    }
}
