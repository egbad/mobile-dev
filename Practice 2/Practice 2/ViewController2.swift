//
//  ViewController2.swift
//  Practice 2
//
//  Created by Егор Бадмаев on 17.03.2024.
//

import UIKit
import SafariServices

final class ViewController2: UIViewController {
    
    @IBAction func linkButtonAAction(_ sender: Any) {
        guard let url = URL(string: "https://mirea.ru") else { return }
        let vc = SFSafariViewController(url: url)
        present(vc, animated: true)
    }
    
    @IBAction func linkButtonBAction(_ sender: Any) {
        guard let url = URL(string: "https://mirea.ru") else { return }
        UIApplication.shared.open(url)
    }
}
