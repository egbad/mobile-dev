//
//  ViewController0.swift
//  Practice 2
//
//  Created by Егор Бадмаев on 17.03.2024.
//

import UIKit

class ViewController0: UIViewController {
    
    override func loadView() {
        super.loadView()
        
        print("loadView \(#fileID)")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("viewDidLoad \(#fileID)")
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        print("viewWillAppear \(#fileID)")
    }
    
    override func viewIsAppearing(_ animated: Bool) {
        super.viewIsAppearing(animated)
        
        print("viewIsAppearing \(#fileID)")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        print("viewDidAppear \(#fileID)")
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        print("viewWillLayoutSubviews \(#fileID)")
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        print("viewDidLayoutSubviews \(#fileID)")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        print("viewWillDisappear \(#fileID)")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        print("viewDidDisappear \(#fileID)")
    }
}
