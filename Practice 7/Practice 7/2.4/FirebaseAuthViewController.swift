//
//  FirebaseAuthViewController.swift
//  Lesson7
//
//  Created by Егор Бадмаев on 24.05.2024.
//

import UIKit
import FirebaseAuth

final class FirebaseAuthViewController: UIViewController {
    
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var uidLabel: UILabel!
    
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var buttonsStackView: UIStackView!
    @IBOutlet weak var firstButton: UIButton!
    @IBOutlet weak var secondButton: UIButton!
    
    private var isLoggedIn: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        buttonsStackView.layer.cornerRadius = 12
        
        Auth.auth().addStateDidChangeListener { [weak self] (_, user) in
            if let user {
                self?.emailLabel.text = "Email: \(user.email ?? ""), Verified: \(user.isEmailVerified)"
                self?.uidLabel.text = "Firebase UID: \(user.uid)"
                
                self?.firstButton.setTitle("Sign Out", for: .normal)
                self?.secondButton.setTitle("Verify Email", for: .normal)
                self?.secondButton.isEnabled = !user.isEmailVerified
            } else {
                self?.emailLabel.text = "Signed Out"
                self?.uidLabel.text = nil
                
                self?.firstButton.setTitle("Sign In", for: .normal)
                self?.secondButton.setTitle("Create Account", for: .normal)
                self?.secondButton.isEnabled = true
            }
            self?.isLoggedIn = user != nil
        }
    }
    
    @IBAction func firstButtonTapped(_ sender: Any) {
        if isLoggedIn {
            signOut()
        } else {
            guard let (email, password) = validateFieldsData() else { return }
            
            signIn(email: email, password: password)
        }
        
    }
    
    @IBAction func secondButtonTapped(_ sender: Any) {
        if isLoggedIn {
            sendEmailVerification()
        } else {
            guard let (email, password) = validateFieldsData() else { return }
            
            createAccount(email: email, password: password)
        }
    }
    
    private func createAccount(email: String, password: String) {
        Auth.auth().createUser(withEmail: email, password: password) { (result, error) in
            if let error = error {
                print(error, error.localizedDescription)
            } else {
                print("createAccount - user UID: ", result?.user.uid ?? "")
            }
        }
    }
    
    private func signIn(email: String, password: String) {
        Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
            if let error = error {
                print(error, error.localizedDescription)
            } else {
                print("signIn - user UID: ", result?.user.uid ?? "")
            }
        }
    }
    
    private func signOut() {
        do {
            try Auth.auth().signOut()
        } catch let error {
            print(error, error.localizedDescription)
        }
    }
    
    private func sendEmailVerification() {
        if let user = Auth.auth().currentUser {
            user.sendEmailVerification { (error) in
                if let error = error {
                    print(error, error.localizedDescription)
                } else {
                    print("sendEmailVerification - user email: \(user.email ?? ""), UID: \(user.uid)")
                }
            }
        }
    }
    
    private func validateFieldsData() -> (email: String, password: String)? {
        guard let email = emailTextField.text, !email.isEmpty,
              let password = passwordTextField.text, !password.isEmpty else {
            print("Заполнены не все поля формы")
            return nil
        }
        
        return (email: email, password: password)
    }
}
