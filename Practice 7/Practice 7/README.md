# Практическая №5

| Socket 1 | Socket 2 | Weather |
|---|---|---|
| ![!](Screens/Socket1.png) | ![!](Screens/Socket2.png) | ![!](Screens/Weather.png) |
| Firebase 1 | Firebase 2 | Firebase 3 |
| ![!](Screens/Firebase1.png) | ![!](Screens/Firebase2.png) | ![!](Screens/Firebase3.png) |
| Firebase 4 | Firebase 5 | Main.storyboard |
| ![!](Screens/Firebase4.png) | ![!](Screens/Firebase5.png) | ![!](Screens/Main.storyboard.png) |
