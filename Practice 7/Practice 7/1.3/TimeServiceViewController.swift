//
//  TimeServiceViewController.swift
//  Practice 7
//
//  Created by Егор Бадмаев on 24.05.2024.
//

import UIKit
import Starscream

final class TimeServiceViewController: UIViewController {
    @IBOutlet weak var label: UILabel!
    
    private var socket: WebSocket!
    
    private lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "EEE, dd MMM yyyy HH:mm:ss z"
        return dateFormatter
    }()
    
    @IBAction func fetchTime(_ sender: UIButton) {
        guard let url = URL(string: "wss://time.cloudflare.com") else { return }
        
        socket = WebSocket(request: URLRequest(url: url))
        socket.delegate = self
        socket.connect()
    }
}

// MARK: - WebSocketDelegate

extension TimeServiceViewController: WebSocketDelegate {
    
    func didReceive(event: WebSocketEvent, client: WebSocketClient) {
        switch event {
        case .error(let error):
            if let error = error as? HTTPUpgradeError,
               case .notAnUpgrade(_, let dictionary) = error {
                var labelText: String
                if let date = self.dateFormatter.date(from: dictionary["Date", default: ""]) {
                    let timeFormatter = DateFormatter()
                    timeFormatter.timeStyle = .short
                    
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateStyle = .long
                    
                    let timeString = timeFormatter.string(from: date)
                    let dateString = dateFormatter.string(from: date)
                    
                    labelText = "Дата: \(dateString), время: \(timeString)"
                } else {
                    labelText = "Error occured"
                }
                DispatchQueue.main.async {
                    self.label.text = labelText
                }
            }
        default:
            break
        }
    }
}
