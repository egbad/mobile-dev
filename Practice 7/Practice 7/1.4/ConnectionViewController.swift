//
//  ConnectionViewController.swift
//  Practice 7
//
//  Created by Егор Бадмаев on 24.05.2024.
//

import UIKit

final class ConnectionViewController: UIViewController {
    
    private struct ViewModel {
        let value: String
        let unit: String
        let imagePath: String
    }
    
    private var dataSource: [ViewModel] = []
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .insetGrouped)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .secondarySystemBackground
        view.addSubview(tableView)
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor)
        ])
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        makeRequest()
    }
    
    private func makeRequest() {
        guard let url = URL(string: "https://api.ipify.org") else { return }
        
        let request = URLRequest(url: url)
        let task = URLSession.shared.dataTask(with: request) { [weak self] data, _, _ in
            guard let self,
                  let data,
                  let ip = String(data: data, encoding: .utf8)
            else {
                return
            }
            
            let components = ip.split(separator: ".")
            let latitude = String(min((Int(components[0]) ?? 0), 89)) + "." + components[1]
            let longitude = String(components[2] + "." + components[3])
            
            guard let url2 = URL(
                string: "https://api.open-meteo.com/v1/forecast?latitude=\(latitude)&longitude=\(longitude)&current_weather=true"
            ) else {
                return
            }
            
            let task2 = URLSession.shared.dataTask(with: URLRequest(url: url2)) { data2, _, _ in
                guard let data2 else { return }
                
                do {
                    let weather = try JSONDecoder().decode(WeatherResponse.self, from: data2)
                    self.dataSource.append(ViewModel(value: latitude, unit: "Latitude", imagePath: "mappin.and.ellipse"))
                    self.dataSource.append(ViewModel(value: longitude, unit: "Longitude", imagePath: "location.north.fill"))
                    self.dataSource.append(ViewModel(value: weather.timezone, unit: "Timezone", imagePath: "clock"))
                    self.dataSource.append(ViewModel(value: String(weather.currentWeather.temperature), unit: weather.currentWeatherUnits.temperature, imagePath: "thermometer.medium"))
                    self.dataSource.append(ViewModel(value: String(weather.currentWeather.windspeed), unit: weather.currentWeatherUnits.windspeed, imagePath: "wind"))
                    self.dataSource.append(ViewModel(value: String(weather.currentWeather.winddirection), unit: weather.currentWeatherUnits.winddirection, imagePath: "location.fill"))
                    self.dataSource.append(ViewModel(value: String(weather.currentWeather.weathercode), unit: weather.currentWeatherUnits.weathercode, imagePath: "cloud.sun.fill"))
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                } catch {
                    print(error, error.localizedDescription)
                }
            }
            task2.resume()
        }
        task.resume()
    }
}

extension ConnectionViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        var configuration = cell.defaultContentConfiguration()
        configuration.text = dataSource[indexPath.row].value
        configuration.secondaryText = dataSource[indexPath.row].unit
        configuration.image = UIImage(systemName: dataSource[indexPath.row].imagePath)
        cell.contentConfiguration = configuration
        return cell
    }
}
