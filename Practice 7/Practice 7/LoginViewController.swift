//
//  LoginViewController.swift
//  Practice 7
//
//  Created by Егор Бадмаев on 25.05.2024.
//

import UIKit
import FirebaseAuth

final class LoginViewController: UIViewController {
    
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Auth.auth().addStateDidChangeListener { [weak self] (_, user) in
            if user != nil {
                self?.presentMainPage()
            } else {
                self?.emailTextField.text = nil
                self?.passwordTextField.text = nil
            }
        }
    }
    
    @IBAction func login(_ sender: Any) {
        guard let email = emailTextField.text, !email.isEmpty,
              let password = passwordTextField.text, !password.isEmpty else {
            print("Заполните все поля формы!")
            return
        }
        
        Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
            if let error = error {
                print(error, error.localizedDescription)
            } else {
                print("login - user UID:", result?.user.uid ?? "")
            }
        }
    }
    
    private func presentMainPage() {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let viewController = storyboard.instantiateViewController(withIdentifier: "main")
        viewController.modalPresentationStyle = .fullScreen
        present(viewController, animated: true)
    }
}
