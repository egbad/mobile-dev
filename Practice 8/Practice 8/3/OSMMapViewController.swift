//
//  OSMMapViewController.swift
//  Practice 8
//
//  Created by Егор Бадмаев on 11.06.2024.
//

import UIKit
import MapKit
import CoreLocation

final class OSMMapViewController: UIViewController {
    
    lazy var mapView: MKMapView = {
        let mapView = MKMapView()
        mapView.delegate = self
        mapView.showsScale = true
        mapView.showsCompass = true
        mapView.showsUserLocation = true
        return mapView
    }()
    
    var tileRenderer: MKTileOverlayRenderer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapView.cameraZoomRange = MKMapView.CameraZoomRange(
            minCenterCoordinateDistance: 1000,
            maxCenterCoordinateDistance: .infinity
        )
        mapView.cameraBoundary = MKMapView.CameraBoundary(coordinateRegion: MKCoordinateRegion(.world))
        
        OSMMapLocation.allCases.forEach {
            let annotation = MKPointAnnotation()
            annotation.title = $0.rawValue
            annotation.subtitle = $0.description
            annotation.coordinate = CLLocationCoordinate2D(latitude: $0.ymkPoint.latitude, longitude: $0.ymkPoint.longitude)
            mapView.addAnnotation(annotation)
        }
        
        let overlay = OSMTileOverlay()
        overlay.canReplaceMapContent = true
        tileRenderer = MKTileOverlayRenderer(tileOverlay: overlay)
        mapView.addOverlay(overlay, level: .aboveLabels)
        
        mapView.frame = view.bounds
        view.addSubview(mapView)
        
        let plusButon = UIButton(
            configuration: .tinted(),
            primaryAction: UIAction(handler: {
                [weak self] _ in
                guard let self else { return }
                
                var region = self.mapView.region
                region.span.latitudeDelta /= 2.0
                region.span.longitudeDelta /= 2.0
                self.mapView.setRegion(region, animated: true)
            })
        )
        plusButon.setTitle("+", for: .normal)
        
        let minusButon = UIButton(
            configuration: .tinted(),
            primaryAction: UIAction(handler: { [weak self] _ in
                guard let self else { return }
                
                var region = self.mapView.region
                region.span.latitudeDelta = min(region.span.latitudeDelta * 2.0, 200)
                region.span.longitudeDelta = min(region.span.longitudeDelta * 2.0, 200)
                self.mapView.setRegion(region, animated: true)
            })
        )
        minusButon.setTitle("-", for: .normal)
        
        [plusButon, minusButon].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview($0)
            
            NSLayoutConstraint.activate([
                $0.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -16)
            ])
        }
        
        NSLayoutConstraint.activate([
            plusButon.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: -24),
            minusButon.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 24),
        ])
    }
}

extension OSMMapViewController: MKMapViewDelegate {
    
    func mapView(
        _ mapView: MKMapView,
        rendererFor overlay: MKOverlay
    ) -> MKOverlayRenderer {
        tileRenderer ?? MKOverlayRenderer()
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard annotation is MKPointAnnotation else { return nil }
        
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "lesson8")
        
        if let annotationView {
            annotationView.annotation = annotation
        } else {
            annotationView = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: "lesson8")
            annotationView?.canShowCallout = true
        }
        
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        guard let annotation = view.annotation as? MKPointAnnotation,
              let value = annotation.title,
              let location = OSMMapLocation(rawValue: value)
        else { return }
        
        let mapPointViewController = MapPointViewController()
        mapPointViewController.configure(with: location)
        if let sheet = mapPointViewController.sheetPresentationController {
            sheet.detents = [.medium()]
        }
        present(mapPointViewController, animated: true)
    }
}
