//
//  OSMTileOverlay.swift
//  Practice 8
//
//  Created by Егор Бадмаев on 11.06.2024.
//

import MapKit

final class OSMTileOverlay: MKTileOverlay {
    
    override func url(forTilePath path: MKTileOverlayPath) -> URL {
        return URL(
            string: "https://tile.openstreetmap.org/\(path.z)/\(path.x)/\(path.y).png"
        ) ?? super.url(forTilePath: path)
    }
}
