//
//  OSMMapLocation.swift
//  Practice 8
//
//  Created by Егор Бадмаев on 11.06.2024.
//

import Foundation
import YandexMapsMobile

enum OSMMapLocation: String, CaseIterable {
    case aviapark = "Авиапарк"
    case supermetal = "Суперметал"
    case oasis = "Оазис"
    
    var ymkPoint: YMKPoint {
        switch self {
        case .aviapark:
            return YMKPoint(latitude: 55.789794, longitude: 37.529667)
        case .supermetal:
            return YMKPoint(latitude: 55.764065, longitude: 37.683365)
        case .oasis:
            return YMKPoint(latitude: 55.728854, longitude: 37.620330)
        }
    }
    
    var description: String {
        switch self {
        case .aviapark:
            return "Очень хороший торговый центр. Рядом с метро. 4 этажа, есть интерактивные карты, чтобы не потеряться. Широкий выбор одежды и обуви, каких только магазинов нет! Ещё внутри расположена очень интересная зона с одеждой, обувью и аксессуарами trend island. Каждый может найти там то, что понравится: и интересные дизайнерские решения, и есть изделия из натуральных материалов!"
        case .supermetal:
            return "Очень интересное пространство, действительно большая часть сделана из металла. Сам бизнес центр закрыт от посещения - только по пропускам. Небольшая зона открыта для посетителей ; которая включает в себя три торговых магазина один магазин сувениров, кофейню и небольшой ряд сидячих мест"
        case .oasis:
            return "Супер современный и комфортабельный офис. Удобно как расположение, так и так и сам БЦ - все необходимое есть как внутри него, так и в ближайшем окружении"
        }
    }
}
