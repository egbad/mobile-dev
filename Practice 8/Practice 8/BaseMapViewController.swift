//
//  BaseMapViewController.swift
//  Practice 8
//
//  Created by Егор Бадмаев on 11.06.2024.
//

import UIKit
import CoreLocation
import YandexMapsMobile

class BaseMapViewController: UIViewController {
    
    lazy var locationManager: CLLocationManager = {
        let locationManager = CLLocationManager()
        locationManager.delegate = self
        return locationManager
    }()
    
    let mapView: YMKMapView = YMKMapView(frame: .zero)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapView.frame = view.bounds
        view.addSubview(mapView)
    }
}

extension BaseMapViewController: CLLocationManagerDelegate {
    
    func locationManager(
        _ manager: CLLocationManager,
        didChangeAuthorization status: CLAuthorizationStatus
    ) {
        if status == .authorizedWhenInUse {
            locationManager.startUpdatingLocation()
        }
    }
}
