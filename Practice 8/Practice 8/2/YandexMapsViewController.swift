//
//  YandexMapsViewController.swift
//  Practice 8
//
//  Created by Егор Бадмаев on 01.06.2024.
//

import UIKit
import YandexMapsMobile

final class YandexMapsViewController: BaseMapViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapView.frame = view.bounds
        view.addSubview(mapView)
        
        let scale = UIScreen.main.scale
        let userLocationLayer = YMKMapKit.sharedInstance().createUserLocationLayer(with: mapView.mapWindow)
        userLocationLayer.setVisibleWithOn(true)
        userLocationLayer.isHeadingEnabled = true
        userLocationLayer.setObjectListenerWith(self)
        userLocationLayer.setAnchorWithAnchorNormal(
            CGPoint(
                x: 0.5 * mapView.frame.size.width * scale,
                y: 0.5 * mapView.frame.size.height * scale
            ),
            anchorCourse: CGPoint(
                x: 0.5 * mapView.frame.size.width * scale,
                y: 0.8 * mapView.frame.size.height * scale
            )
        )
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        YMKMapKit.sharedInstance().onStart()
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        YMKMapKit.sharedInstance().onStop()
    }
}

extension YandexMapsViewController: YMKUserLocationObjectListener {
    
    func onObjectAdded(with view: YMKUserLocationView) {
        let pinPlacemark = view.pin.useCompositeIcon()
        
        pinPlacemark.setIconWithName(
            "icon",
            image: UIImage(systemName: "mappin")!,
            style: YMKIconStyle(
                anchor: CGPoint(x: 0.5, y: 0.5) as NSValue,
                rotationType: YMKRotationType.rotate.rawValue as NSNumber,
                zIndex: 0,
                flat: true,
                visible: true,
                scale: 1.0,
                tappableArea: nil
            )
        )
        
        view.accuracyCircle.fillColor = UIColor.systemBlue
    }
    
    func onObjectRemoved(with view: YMKUserLocationView) {
    }
    
    func onObjectUpdated(with view: YMKUserLocationView, event: YMKObjectEvent) {
    }
}
