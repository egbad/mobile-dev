//
//  YandexDriverViewController.swift
//  Practice 8
//
//  Created by Егор Бадмаев on 08.06.2024.
//

import UIKit
import YandexMapsMobile
import CoreLocation

final class YandexDriverViewController: BaseMapViewController {
    
    var currentLocation: YMKPoint?
    var drivingSession: YMKDrivingSession?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let cameraPosition = YMKCameraPosition(
            target: YMKPoint(latitude: 55.755864, longitude: 37.617698),
            zoom: 15.0,
            azimuth: 0.0,
            tilt: 0.0
        )
        mapView.mapWindow.map.move(
            with: cameraPosition,
            animation: YMKAnimation(type: YMKAnimationType.smooth, duration: 0.25)
        )
        
        locationManager.delegate = self
        if locationManager.authorizationStatus != .authorizedWhenInUse {
            locationManager.requestWhenInUseAuthorization()
        } else {
            locationManager.startUpdatingLocation()
        }
    }
    
    func submitRequest() {
        var requestPoints: [YMKRequestPoint] = []
        
        if let currentLocation {
            requestPoints.append(
                YMKRequestPoint(
                    point: YMKPoint(latitude: currentLocation.latitude, longitude: currentLocation.longitude),
                    type: .waypoint,
                    pointContext: nil,
                    drivingArrivalPointId: nil
                )
            )
        }
        
        requestPoints.append(
            YMKRequestPoint(
                point: OSMMapLocation.aviapark.ymkPoint,
                type: .waypoint,
                pointContext: nil,
                drivingArrivalPointId: nil
            )
        )
        
        let drivingRouter = YMKDirectionsFactory.instance().createDrivingRouter(withType: .combined)
        let drivingOptions = YMKDrivingOptions()
        drivingOptions.routesCount = 2
        drivingSession = drivingRouter.requestRoutes(
            with: requestPoints,
            drivingOptions: drivingOptions,
            vehicleOptions: YMKDrivingVehicleOptions(),
            routeHandler: onDrivingRoutes
        )
    }
    
    func onDrivingRoutes(drivingRoutes: [YMKDrivingRoute]?, error: Error?) {
        if let error {
            onDrivingRoutesError(error: error)
            return
        }
        
        for (index, route) in (drivingRoutes ?? []).enumerated() {
            mapView.mapWindow.map.mapObjects
                .addPolyline(with: route.geometry)
                .setStrokeColorWith(index.isMultiple(of: 2) ? UIColor.red : UIColor.green)
        }
        
        let placemark = mapView.mapWindow.map.mapObjects.addPlacemark()
        placemark.geometry = OSMMapLocation.aviapark.ymkPoint
        placemark.addTapListener(with: self)
        placemark.setTextWithText(
            OSMMapLocation.aviapark.rawValue,
            style: YMKTextStyle(
                size: 8.0,
                color: .black,
                outlineWidth: 1.0,
                outlineColor: .white,
                placement: .right,
                offset: 2.0,
                offsetFromIcon: true,
                textOptional: false
            )
        )
        
        let compositeIcon = placemark.useCompositeIcon()
        compositeIcon.setIconWithName(
            "pin",
            image: UIImage(systemName: "mappin.circle")!,
            style: YMKIconStyle(
                anchor: NSValue(cgPoint: CGPoint(x: 0.5, y: 1.0)),
                rotationType: nil,
                zIndex: 1,
                flat: true,
                visible: true,
                scale: 1.0,
                tappableArea: nil
            )
        )
    }
    
    func onDrivingRoutesError(error: Error) {
        print(error.localizedDescription)
    }
}

extension YandexDriverViewController: YMKMapObjectTapListener {
    
    func onMapObjectTap(with mapObject: YMKMapObject, point: YMKPoint) -> Bool {
        DispatchQueue.main.async {
            let mapPointViewController = MapPointViewController()
            mapPointViewController.configure(with: .aviapark)
            if let sheet = mapPointViewController.sheetPresentationController {
                sheet.detents = [.medium()]
            }
            self.present(mapPointViewController, animated: true)
        }
        return true
    }
}

extension YandexDriverViewController {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else { return }
        let coordinate = location.coordinate
        currentLocation = YMKPoint(latitude: coordinate.latitude, longitude: coordinate.longitude)
        submitRequest()
    }
}
