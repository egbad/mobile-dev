//
//  EmployeeDao.swift
//  Practice 6
//
//  Created by Егор Бадмаев on 24.05.2024.
//

import CoreData

protocol EmployeeDao {
    func getAll() -> [Employee]
    func getById(_ id: Int) -> Employee?
    func insert(id: Int, name: String, salary: Int)
    func update(_ employee: Employee)
    func delete(_ employee: Employee)
}

final class EmployeeService: EmployeeDao {
    
    static let shared: EmployeeDao = EmployeeService()
    
    // MARK: - Private Properties
    
    private lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "Entity")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    private lazy var viewContext: NSManagedObjectContext = persistentContainer.viewContext
    
    // MARK: - EmployeeDao
    
    func getAll() -> [Employee] {
        do {
            let all = try viewContext.fetch(Employee.fetchRequest())
            return all
        } catch {
            fatalError("\(error.localizedDescription)")
        }
    }
    
    func getById(_ id: Int) -> Employee? {
        let request: NSFetchRequest<Employee> = Employee.fetchRequest()
        request.predicate = NSPredicate(format: "id == %d", Int64(id))
        
        do {
            let result = try viewContext.fetch(request)
            return result.first
        } catch {
            print(error, error.localizedDescription)
            return nil
        }
    }
    
    func insert(id: Int, name: String, salary: Int) {
        let employee = Employee(context: viewContext)
        employee.id = Int64(id)
        employee.name = name
        employee.salary = Int64(salary)
        saveContext()
    }
    
    func update(_ employee: Employee) {
        saveContext()
    }
    
    func delete(_ employee: Employee) {
        viewContext.delete(employee)
        saveContext()
    }
    
    func saveContext() {
        do {
            try viewContext.save()
        } catch {
            print(error, error.localizedDescription)
        }
    }
}

