//
//  EmployeeViewController.swift
//  Lesson6
//
//  Created by Егор Бадмаев on 14.05.2024.
//

import UIKit

final class EmployeeViewController: UIViewController {
    
    private let employeeDao: EmployeeDao = EmployeeService.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        employeeDao.insert(id: 1, name: "John Smith", salary: 10000)
        let employees = employeeDao.getAll()
        let employee = employeeDao.getById(1)
        employee?.salary = 20000
        
        if let employee {
            employeeDao.update(employee)
        }
        print(employee?.name, employee?.salary)
    }
}
