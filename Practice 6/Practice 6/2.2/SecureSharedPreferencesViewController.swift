//
//  SecureSharedPreferencesViewController.swift
//  Lesson6
//
//  Created by Егор Бадмаев on 14.05.2024.
//

import UIKit

final class SecureSharedPreferencesViewController: UIViewController {
    
    @IBOutlet weak var poetTextField: UITextField!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let query: [CFString: Any] = [
            kSecClass: kSecClassGenericPassword,
            kSecAttrAccount: "poet",
            kSecReturnData: kCFBooleanTrue as Any
        ]
        var result: AnyObject?
        let status = SecItemCopyMatching(query as CFDictionary, &result)
        
        guard status == errSecSuccess else {
            return
        }
        
        if let resultData = result as? Data {
            poetTextField.text = String(data: resultData, encoding: .utf8)
        }
    }
    
    @IBAction func saveButtonTapped(_ sender: Any) {
        guard let poetryName = poetTextField.text,
              !poetryName.isEmpty,
              let data = poetryName.data(using: .utf8)
        else {
            return
        }
        let query: [CFString: Any] = [
            kSecClass: kSecClassGenericPassword,
            kSecAttrAccount: "poet",
            kSecValueData: data
        ]
        
        let status = SecItemAdd(query as CFDictionary, nil)
        guard status != errSecDuplicateItem else {
            return
        }
        guard status == errSecSuccess else {
            return
        }
    }
}
