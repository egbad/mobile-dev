//
//  InternalStorageViewController.swift
//  Practice 6
//
//  Created by Егор Бадмаев on 24.05.2024.
//

import UIKit

class InternalStorageViewController: UIViewController {
    
    @IBOutlet weak var textView: UITextView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            do {
                let fileURL = dir.appendingPathComponent("history.txt")
                let content = try String(contentsOf: fileURL, encoding: .utf8)
                textView.text = content
            } catch {
                print(error, error.localizedDescription)
            }
        }
    }
    
    @IBAction func submit(_ sender: Any) {
        guard let text = textView.text,
              !text.isEmpty
        else {
            return
        }
        
        guard let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first 
        else {
            return
        }
        
        do {
            let fileURL = dir.appendingPathComponent("history.txt")
            try text.write(to: fileURL, atomically: false, encoding: .utf8)
        } catch {
            print(error, error.localizedDescription)
        }
    }
}
