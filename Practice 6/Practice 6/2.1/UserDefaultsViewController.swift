//
//  UserDefaultsViewController.swift
//  Practice 6
//
//  Created by Егор Бадмаев on 14.05.2024.
//

import UIKit

final class UserDefaultsViewController: UIViewController {
    
    @IBOutlet weak var groupTextField: UITextField!
    
    @IBOutlet weak var positionTextField: UITextField!
    
    @IBOutlet weak var filmTextField: UITextField!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        groupTextField.text = UserDefaults.standard.string(forKey: "GROUP")
        positionTextField.text = UserDefaults.standard.string(forKey: "NUMBER")
        filmTextField.text = UserDefaults.standard.string(forKey: "FAVORITE_MOVIE")
    }
    
    @IBAction func submit(_ sender: Any) {
        UserDefaults.standard.set(
            groupTextField.text,
            forKey: "GROUP"
        )
        UserDefaults.standard.set(
            Int(positionTextField.text ?? ""),
            forKey: "NUMBER"
        )
        UserDefaults.standard.set(filmTextField.text, forKey: "FAVORITE_MOVIE")
    }
}

