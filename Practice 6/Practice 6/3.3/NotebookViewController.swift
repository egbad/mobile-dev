//
//  NotebookViewController.swift
//  Lesson6
//
//  Created by Егор Бадмаев on 14.05.2024.
//

import UIKit

final class NotebookViewController: UIViewController {
    
    @IBOutlet weak var fileNameTextField: UITextField!
    
    @IBOutlet weak var quoteTextField: UITextField!
    
    private var documentsDirectory: URL {
        FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
    }
    
    @IBAction func submitData(_ sender: Any) {
        guard let fileName = fileNameTextField.text, !fileName.isEmpty,
              let quote = quoteTextField.text, !quote.isEmpty 
        else {
            return
        }
        let fileURL = documentsDirectory.appendingPathComponent(fileName)
        do {
            try quote.write(to: fileURL, atomically: true, encoding: .utf8)
        } catch {
            print(error, error.localizedDescription)
        }
    }
    
    @IBAction func loadData(_ sender: Any) {
        guard let fileName = fileNameTextField.text,
              !fileName.isEmpty 
        else {
            return
        }
        let fileURL = documentsDirectory.appendingPathComponent(fileName)
        do {
            let quote = try String(contentsOf: fileURL, encoding: .utf8)
            quoteTextField.text = quote
        } catch {
            print(error, error.localizedDescription)
        }
    }
}
