//
//  ProfileViewController.swift
//  Lesson6
//
//  Created by Егор Бадмаев on 14.05.2024.
//

import UIKit
import CryptoKit

final class ProfileViewController: UIViewController {
    
    @IBOutlet weak var statusTextField: UITextField!
    
    @IBOutlet weak var themeSwitch: UISwitch!
    
    @IBOutlet weak var volumeSlider: UISlider!
    
    @IBOutlet weak var decryptButton: UIBarButtonItem!
    
    @IBOutlet weak var encryptButton: UIBarButtonItem!
    
    private var documentsDirectory: URL {
        FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
    }
    
    private let key = SymmetricKey(size: .bits256)
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        statusTextField.text = UserDefaults.standard.string(forKey: "profileStatus")
        themeSwitch.isOn = UserDefaults.standard.bool(forKey: "profileDarkTheme")
        volumeSlider.value = UserDefaults.standard.float(forKey: "profileVolume")
        updateBarButtons()
    }
    
    @IBAction func saveSettings(_ sender: Any) {
        if let status = statusTextField.text {
            UserDefaults.standard.set(status, forKey: "profileStatus")
        }
        UserDefaults.standard.set(themeSwitch.isOn, forKey: "profileDarkTheme")
        UserDefaults.standard.set(volumeSlider.value, forKey: "profileVolume")
    }
    
    @IBAction func encryptText(_ sender: Any) {
        guard let status = statusTextField.text,
              !status.isEmpty
        else {
            print("Не валидные данные - \(statusTextField.text)")
            return
        }
        let encrypt = encrypt(status, key: key)
        statusTextField.text = encrypt
        UserDefaults.standard.set(true, forKey: "profileIsStatusCrypted")
        updateBarButtons()
        saveToFile(text: encrypt ?? "", fileName: "encrypt.txt")
    }
    
    private func encrypt(_ message: String, key: SymmetricKey) -> String {
        guard let messageData = message.data(using: .utf8) else {
            return ""
        }
        
        do {
            let sealedBox = try AES.GCM.seal(messageData, using: key)
            let encryptedData = sealedBox.combined
            return encryptedData?.base64EncodedString() ?? ""
        } catch {
            print("Encryption failed: \(error.localizedDescription)")
            return ""
        }
    }
    
    @IBAction func decryptText(_ sender: Any) {
        let content = readFile(fileName: "encrypt.txt")
        let decrypt = decrypt(content, key: key)
        statusTextField.text = decrypt
        UserDefaults.standard.set(false, forKey: "profileIsStatusCrypted")
        updateBarButtons()
    }
    
    private func decrypt(_ encryptedString: String, key: SymmetricKey) -> String {
        guard let encryptedData = Data(base64Encoded: encryptedString)
        else {
            return ""
        }
        
        do {
            let sealedBox = try AES.GCM.SealedBox(combined: encryptedData)
            let decryptedData = try AES.GCM.open(sealedBox, using: key)
            return String(data: decryptedData, encoding: .utf8) ?? ""
        } catch {
            print("Decryption failed: \(error.localizedDescription)")
            return ""
        }
    }
    
    private func saveToFile(text: String, fileName: String) {
        do {
            let fileURL = documentsDirectory.appendingPathComponent(fileName)
            try text.write(to: fileURL, atomically: true, encoding:.utf8)
            print("Text saved to \(fileURL.path)")
        } catch {
            print("Error saving text: \(error) \(error.localizedDescription)")
        }
    }
    
    private func readFile(fileName: String) -> String {
        do {
            let fileURL = documentsDirectory.appendingPathComponent(fileName)
            let content = try String(contentsOf: fileURL, encoding:.utf8)
            return content
        } catch {
            print("Error reading file: \(error) \(error.localizedDescription)")
            return "Unable to read file."
        }
    }
    
    private func updateBarButtons() {
        let isStatusCrypted = UserDefaults.standard.bool(forKey: "profileIsStatusCrypted")
        decryptButton.isEnabled = isStatusCrypted
        encryptButton.isEnabled = !isStatusCrypted
    }
}
