//
//  ButtonClickerViewController.swift
//  Lesson-1
//
//  Created by Егор Бадмаев on 01.03.2024.
//

import UIKit

final class ButtonClickerViewController: UIViewController {
    
    @IBOutlet weak var textView: UITextView!
    
    @IBOutlet weak var whoAmIButton: UIButton!
    
    @IBOutlet weak var itIsNotMeButton: UIButton!
    
    @IBOutlet weak var checkbox: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        whoAmIButton.addTarget(self, action: #selector(whoAmIButtonAction), for: .touchUpInside)
    }
    
    @objc func whoAmIButtonAction() {
        textView.text = "Мой номер по списку №2"
        checkbox.isOn = false
    }
    
    @IBAction func itIsNotMeButtonIBAction(_ sender: Any) {
        textView.text = "Это не я сделал"
        checkbox.isOn = true
    }
}
