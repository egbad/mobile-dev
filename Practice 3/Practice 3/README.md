| Navigation 1 | Navigation 2 | Favorite Book 1 |
|---|---|---|
| ![!](Screens/nav1.png) | ![!](Screens/nav2.png) | ![!](Screens/book1.png) |
| Favorite Book 2 | Favorite Book 3 | Favorite Book 4 |
| ![!](Screens/book2.png) | ![!](Screens/book3.png) | ![!](Screens/book4.png) |
| System Intents 1 | System Intents 2 | System Intents 3 |
| ![!](Screens/system1.png) | ![!](Screens/system2.png) | ![!](Screens/system3.png) |
| Fragment 1 | Fragment 2 | Fragment 3 |
| ![!](Screens/fragment1.png) | ![!](Screens/fragment2.png) | ![!](Screens/fragment3.png) |
| Fragment 4 | Fragment 5 | Fragment 6 |
| ![!](Screens/fragment4.png) | ![!](Screens/fragment5.png) | ![!](Screens/fragment6.png) |
| Fragment 7 | Reminder 1 | Reminder 2 |
| ![!](Screens/fragment7.png) | ![!](Screens/reminder.png) | ![!](Screens/reminder2.png) |
