//
//  FirstFragmentViewController.swift
//  Practice 3
//
//  Created by Егор Бадмаев on 31.03.2024.
//

import UIKit

final class FirstFragmentViewController: UIViewController {
    private let label: UILabel = {
        let label = UILabel()
        label.text = "Hello blank fragment"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    private let button: UIButton = {
        let button = UIButton()
        button.setTitle("Fragment 2", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.isHidden = UIDevice.current.orientation.isLandscape
        return button
    }()
    var secondFragment: UIViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemPurple
        view.addSubview(label)
        view.addSubview(button)
        NSLayoutConstraint.activate([
            label.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            label.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            label.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
            button.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            button.topAnchor.constraint(equalTo: label.bottomAnchor, constant: 16),
            button.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
        ])
        button.addTarget(self, action: #selector(buttonDidTapped), for: .touchUpInside)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        button.isHidden = UIDevice.current.orientation.isLandscape
    }
    
    @objc
    private func buttonDidTapped() {
        if let splitViewController, let secondFragment {
            splitViewController.showDetailViewController(secondFragment, sender: nil)
        }
    }
}
