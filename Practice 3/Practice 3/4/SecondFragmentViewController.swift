//
//  SecondFragmentViewController.swift
//  Practice 3
//
//  Created by Егор Бадмаев on 31.03.2024.
//

import UIKit

final class SecondFragmentViewController: UIViewController {
    private let label: UILabel = {
        let label = UILabel()
        label.text = "Second Fragment"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemMint
        view.addSubview(label)
        NSLayoutConstraint.activate([
            label.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            label.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            label.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor)
        ])
    }
}
