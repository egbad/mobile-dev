//
//  FragmentViewController.swift
//  Practice 3
//
//  Created by Егор Бадмаев on 31.03.2024.
//

import UIKit

final class FragmentViewController: UISplitViewController, UISplitViewControllerDelegate {
    override func loadView() {
        super.loadView()
        let firstFragment = FirstFragmentViewController()
        let secondFragment = UINavigationController(rootViewController: SecondFragmentViewController())
        firstFragment.secondFragment = secondFragment
        viewControllers = [
            UINavigationController(rootViewController: firstFragment),
            secondFragment
        ]
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        preferredDisplayMode = .oneBesideSecondary
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
}
