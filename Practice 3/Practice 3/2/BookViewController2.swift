//
//  BookViewController2.swift
//  Practice 3
//
//  Created by Егор Бадмаев on 31.03.2024.
//

import UIKit

protocol BookDelegate: AnyObject {
    func setFavoriteBook(_ name: String)
}

final class BookViewController2: UIViewController {
    var favoriteBook: String?
    var delegate: BookDelegate?
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var textView: UITextView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let favoriteBook {
            textView.text = "Любимая книга разработчика - " + favoriteBook
        } else {
            textView.text = "Тут появится название вашей любимой книги!"
        }
    }
    
    @IBAction func submitButtonAction(_ sender: Any) {
        guard let text = textField.text,
              !text.isEmpty 
        else {
            return
        }
        favoriteBook = text
        textView.text = "Любимая книга разработчика - " + text
        delegate?.setFavoriteBook(text)
    }
}
