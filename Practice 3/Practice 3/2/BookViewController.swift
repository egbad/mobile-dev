//
//  BookViewController.swift
//  Practice 3
//
//  Created by Егор Бадмаев on 31.03.2024.
//

import UIKit

/*
 FavoriteBook
 */

final class BookViewController: UIViewController {
    var favoriteBook: String?
    @IBOutlet weak var textView: UITextView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let favoriteBook {
            textView.text = "Название Вашей любимой книги: " + favoriteBook
        } else {
            textView.text = "Тут появится название вашей любимой книги!"
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier == "segue2",
              let destination = segue.destination as? BookViewController2
        else { return }
        destination.delegate = self
        destination.favoriteBook = favoriteBook
    }
}

extension BookViewController: BookDelegate {
    func setFavoriteBook(_ name: String) {
        favoriteBook = name
        textView.text = "Название Вашей любимой книги: " + name
    }
}
