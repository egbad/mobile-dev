//
//  ViewController.swift
//  Practice 3
//
//  Created by Егор Бадмаев on 31.03.2024.
//

import UIKit

final class ViewController: UIViewController {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier == "segue",
              let destination = segue.destination as? ViewController2
        else { return }
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let stringDate = formatter.string(from: Date())
        destination.output = "КВАДРАТ ЗНАЧЕНИЯ МОЕГО НОМЕРА ПО СПИСКУ В ГРУППЕ СОСТАВЛЯЕТ \(4), А ТЕКУЩЕЕ ВРЕМЯ \(stringDate)"
    }
}
