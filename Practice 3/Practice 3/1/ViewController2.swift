//
//  ViewController2.swift
//  Practice 3
//
//  Created by Егор Бадмаев on 31.03.2024.
//

import UIKit

final class ViewController2: UIViewController {
    
    var output: String?

    @IBOutlet weak var textView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let output {
            textView.text = output
        }
    }
}
