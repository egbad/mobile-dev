//
//  IntentViewController.swift
//  Practice 3
//
//  Created by Егор Бадмаев on 31.03.2024.
//

import UIKit

final class IntentViewController: UIViewController {
    
    @IBAction func callButtonAction(_ sender: Any) {
        guard let url = URL(string: "tel://89811112233"),
              UIApplication.shared.canOpenURL(url)
        else { return }
        UIApplication.shared.open(url)
    }
    
    @IBAction func browserButtonAction(_ sender: Any) {
        guard let url = URL(string: "http://developer.android.com"),
              UIApplication.shared.canOpenURL(url)
        else { return }
        UIApplication.shared.open(url)
    }
    
    @IBAction func mapButtonAction(_ sender: Any) {
        guard let url = URL(string: "maps://?q=\(55.749479),\(37.613944)"),
              UIApplication.shared.canOpenURL(url)
        else { return }
        UIApplication.shared.open(url)
    }
}
