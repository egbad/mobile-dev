//
//  ReminderTableViewCell.swift
//  Practice 3
//
//  Created by Егор Бадмаев on 31.03.2024.
//

import UIKit

final class ReminderTableViewCell: UITableViewCell {
    private let labelsArray = ["Время дедлайна", "Дата дедлайна"]
    private lazy var label: UILabel = {
        let label = UILabel()
        label.isHidden = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    private lazy var textField: UITextField = {
        let textField = UITextField()
        textField.isHidden = true
        textField.placeholder = "Введите задание"
        textField.addTarget(self, action: #selector(textFieldChange), for: .allEditingEvents)
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    private lazy var datePicker: UIDatePicker = {
        let datePicker = UIDatePicker()
        datePicker.isHidden = true
        datePicker.datePickerMode = .date
        datePicker.addTarget(self, action: #selector(datePickerChange), for: .allEditingEvents)
        datePicker.translatesAutoresizingMaskIntoConstraints = false
        return datePicker
    }()
    private lazy var timePicker: UIDatePicker = {
        let datePicker = UIDatePicker()
        datePicker.isHidden = true
        datePicker.datePickerMode = .time
        datePicker.addTarget(self, action: #selector(timePickerChange), for: .allEditingEvents)
        datePicker.translatesAutoresizingMaskIntoConstraints = false
        return datePicker
    }()
    
    private lazy var textView: UITextView = {
        let textView = UITextView()
        textView.isHidden = true
        textView.isScrollEnabled = false
        textView.delegate = self
        textView.font = UIFont.systemFont(ofSize: 16)
        textView.text = "Введите описание"
        textView.backgroundColor = .clear
        textView.textColor = .placeholderText
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.resignFirstResponder()
        return textView
    }()
    private lazy var sdoButton: UIButton = {
        let button = UIButton()
        button.isHidden = true
        button.layer.cornerRadius = 12
        button.backgroundColor = .systemBlue
        button.setTitle("Перейти в СДО", for: .normal)
        button.addTarget(nil, action: #selector(ReminderViewController.sdoButtonTapped), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupView()
        setConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        contentView.addSubview(label)
        contentView.addSubview(textField)
        textField.delegate = self
        contentView.addSubview(textView)
        
        contentView.addSubview(datePicker)
        contentView.addSubview(timePicker)
        contentView.addSubview(sdoButton)
    }
    
    private func setConstraints() {
        NSLayoutConstraint.activate([
            label.leadingAnchor.constraint(equalTo: contentView.layoutMarginsGuide.leadingAnchor),
            label.centerYAnchor.constraint(equalTo: centerYAnchor),
            
            textField.leadingAnchor.constraint(equalTo: contentView.layoutMarginsGuide.leadingAnchor),
            textField.trailingAnchor.constraint(equalTo: contentView.layoutMarginsGuide.trailingAnchor),
            textField.centerYAnchor.constraint(equalTo: centerYAnchor),
            
            textView.leadingAnchor.constraint(equalTo: contentView.layoutMarginsGuide.leadingAnchor, constant: -4),
            textView.trailingAnchor.constraint(equalTo: contentView.layoutMarginsGuide.trailingAnchor),
            textView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 6),
            textView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -6),
            
            timePicker.leadingAnchor.constraint(equalTo: label.trailingAnchor, constant: 16),
            timePicker.trailingAnchor.constraint(equalTo: contentView.layoutMarginsGuide.trailingAnchor),
            timePicker.centerYAnchor.constraint(equalTo: centerYAnchor),
            
            datePicker.leadingAnchor.constraint(equalTo: label.trailingAnchor, constant: 16),
            datePicker.trailingAnchor.constraint(equalTo: contentView.layoutMarginsGuide.trailingAnchor),
            datePicker.centerYAnchor.constraint(equalTo: centerYAnchor),
            
            sdoButton.leadingAnchor.constraint(equalTo: contentView.layoutMarginsGuide.leadingAnchor),
            sdoButton.trailingAnchor.constraint(equalTo: contentView.layoutMarginsGuide.trailingAnchor),
            sdoButton.centerYAnchor.constraint(equalTo: centerYAnchor)
        ])
    }
    
    public func configure(indexPath: IndexPath) {
        if indexPath == [0, 0] {
            textField.isHidden = false
        }
        if indexPath == [0, 1] {
            textView.isHidden = false
        }
        if indexPath == [1, 0] {
            timePicker.isHidden = false
            label.isHidden = false
            label.text = labelsArray[indexPath.row]
        }
        if indexPath == [1, 1] {
            datePicker.isHidden = false
            label.isHidden = false
            label.text = labelsArray[indexPath.row]
        }
        if indexPath == [1, 2] {
            sdoButton.isHidden = false
        }
    }
}

extension ReminderTableViewCell: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    @objc private func textFieldChange(_ sender: UITextField) {
        print(sender.text)
    }
    
    @objc private func datePickerChange(_ sender: UIDatePicker) {
        print(sender.date)
    }
    
    @objc private func timePickerChange(_ sender: UIDatePicker) {
        print(sender.date)
    }
}

extension ReminderTableViewCell: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        print(textView.text)
    }
    
    // adding placeholder to the TextView
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == .placeholderText {
            textView.text = nil
            textView.textColor = .label
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Введите описание"
            textView.textColor = .placeholderText
        }
    }
}
