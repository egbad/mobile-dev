//
//  TabBarAssembly.swift
//  MireaProject
//
//  Created by Егор Бадмаев on 01.06.2024.
//

import UIKit

final class TabBarAssembly {
    
    private var viewControllers: [UIViewController] = []
    private let tabBarController = UITabBarController()
    private let storyboard = UIStoryboard(name: "Storyboard", bundle: Bundle.main)
    
    func start() -> UITabBarController {
        addController(ReminderViewController(), itemName: "Reminder", itemImageName: "star")
        
        let profileViewController = storyboard.instantiateViewController(withIdentifier: "p51")
        addController(profileViewController, itemName: "Profile", itemImageName: "person.fill")
        let audioViewController = storyboard.instantiateViewController(withIdentifier: "p52")
        addController(audioViewController, itemName: "Recorder", itemImageName: "square")
        addController(CompassViewController(), itemName: "Compass", itemImageName: "location")
        
        let profileViewController2 = storyboard.instantiateViewController(withIdentifier: "p6")
        addController(profileViewController2, itemName: "Profile 2", itemImageName: "person.fill")
        
        let loginViewController2 = storyboard.instantiateViewController(withIdentifier: "p71")
        addController(loginViewController2, itemName: "Firebase", itemImageName: "person")
        
        addController(MapsViewController(), itemName: "Maps", itemImageName: "map")
        
        tabBarController.setViewControllers(viewControllers, animated: false)
        return tabBarController
    }
    
    private func addController(
        _ viewController: UIViewController,
        itemName: String,
        itemImageName: String
    ) {
        viewControllers.append(
            createController(viewController: viewController, itemName: itemName, itemImageName: itemImageName)
        )
    }
    
    private func createController(
        viewController: UIViewController,
        itemName: String,
        itemImageName: String
    ) -> UINavigationController {
        let navigationController = UINavigationController(rootViewController: viewController)
        navigationController.tabBarItem = UITabBarItem(
            title: itemName,
            image: UIImage(systemName: itemImageName),
            tag: 0
        )
        navigationController.navigationBar.prefersLargeTitles = true
        return navigationController
    }
}
