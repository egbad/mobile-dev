//
//  MapsViewController.swift
//  MireaProject
//
//  Created by Егор Бадмаев on 13.06.2024.
//

import UIKit
import YandexMapsMobile
import CoreLocation

final class MapsViewController: UIViewController {
    
    private let mapView: YMKMapView = YMKMapView(frame: .zero)
    private lazy var trafficButton: UISwitch = {
        let button = UISwitch()
        button.addTarget(self, action: #selector(onSwitchTraffic), for: .valueChanged)
        return button
    }()
    private let trafficLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = .systemGreen
        label.font = UIFont.systemFont(ofSize: 33, weight: .bold)
        return label
    }()
    
    var locations: [YMKMapObject: MapLocation] = [:]
    
    var currentLocation: YMKPoint?
    var trafficLayer : YMKTrafficLayer?
    var drivingSession: YMKDrivingSession?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapView.frame = view.bounds
        view.addSubview(mapView)
        
        [trafficButton, trafficLabel].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview($0)
        }
        NSLayoutConstraint.activate([
            trafficLabel.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -16),
            trafficLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 16),
            
            trafficButton.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -16),
            trafficButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -16)
        ])
        
        let cameraPosition = YMKCameraPosition(
            target: YMKPoint(latitude: 55.755864, longitude: 37.617698),
            zoom: 15.0,
            azimuth: 0.0,
            tilt: 0.0
        )
        mapView.mapWindow.map.move(
            with: cameraPosition,
            animation: YMKAnimation(type: YMKAnimationType.smooth, duration: 0.25)
        )
        trafficLayer = YMKMapKit.sharedInstance().createTrafficLayer(with: mapView.mapWindow)
        trafficLayer?.addTrafficListener(withTrafficListener: self)
        
        MapLocation.allCases.forEach {
            let placemark = mapView.mapWindow.map.mapObjects.addPlacemark()
            placemark.geometry = $0.ymkPoint
            placemark.addTapListener(with: self)
            placemark.setTextWithText(
                $0.rawValue,
                style: YMKTextStyle(
                    size: 8.0,
                    color: .black,
                    outlineWidth: 1.0,
                    outlineColor: .white,
                    placement: .right,
                    offset: 2.0,
                    offsetFromIcon: true,
                    textOptional: false
                )
            )
            locations[placemark] = $0
            
            let compositeIcon = placemark.useCompositeIcon()
            compositeIcon.setIconWithName(
                "pin",
                image: UIImage(systemName: $0.iconName)!,
                style: YMKIconStyle(
                    anchor: NSValue(cgPoint: CGPoint(x: 0.5, y: 1.0)),
                    rotationType: nil,
                    zIndex: 1,
                    flat: true,
                    visible: true,
                    scale: 1.0,
                    tappableArea: nil
                )
            )
        }
    }
    
    @objc private func onSwitchTraffic(_ sender: Any) {
        if trafficButton.isOn {
            trafficLabel.text = "0"
            trafficLabel.backgroundColor = UIColor.systemBackground
            trafficLayer?.setTrafficVisibleWithOn(true)
        } else {
            trafficLabel.text = ""
            trafficLabel.backgroundColor = UIColor.systemGray
            trafficLayer?.setTrafficVisibleWithOn(false)
        }
    }
}

extension MapsViewController: YMKMapObjectTapListener, YMKTrafficDelegate {
    
    func onTrafficChanged(with trafficLevel: YMKTrafficLevel?) {
    }
    
    func onTrafficLoading() {
    }
    
    func onTrafficExpired() {
    }
    
    
    func onMapObjectTap(with mapObject: YMKMapObject, point: YMKPoint) -> Bool {
        DispatchQueue.main.async {
            if let location = self.locations[mapObject] {
                let mapPointViewController = MapPointViewController()
                CLGeocoder().reverseGeocodeLocation(
                    CLLocation(latitude: point.latitude, longitude: point.longitude),
                    preferredLocale: Locale(identifier: "ru")
                ) { (placemarks, error) in
                    if let error = error {
                        print("Ошибка: \(error.localizedDescription)")
                        return
                    }
                    
                    if let placemark = placemarks?.first {
                        if let address = placemark.name {
                            mapPointViewController.configure(with: location, address: address)
                            if let sheet = mapPointViewController.sheetPresentationController {
                                sheet.detents = [.medium()]
                            }
                            self.present(mapPointViewController, animated: true)
                        }
                    }
                }
            }
        }
        return true
    }
}
