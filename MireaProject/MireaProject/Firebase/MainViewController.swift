//
//  MainViewController.swift
//  Practice 7
//
//  Created by Егор Бадмаев on 25.05.2024.
//

import UIKit
import FirebaseAuth

struct Post: Codable {
    let userId: Int
    let id: Int
    let title: String
    let body: String
}

final class MainViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fetchData(urlString: "https://jsonplaceholder.typicode.com/posts/1") { result in
            switch result {
            case .success(let post):
                DispatchQueue.main.async {
                    self.titleLabel.text = post.title
                    self.bodyLabel.text = post.body
                    self.subtitleLabel.text = "Пост №\(post.id) был оставлен пользователем №\(post.userId)"
                }
            case .failure(let error):
                DispatchQueue.main.async {
                    self.titleLabel.text = "Error"
                    self.bodyLabel.text = "\(error.localizedDescription)"
                    self.subtitleLabel.text = nil
                }
            }
        }
    }
    
    @IBAction func logout(_ sender: Any) {
        do {
            try Auth.auth().signOut()
            dismiss(animated: true)
        } catch {
            print(error, error.localizedDescription)
        }
    }

    func fetchData(urlString: String, completion: @escaping (Result<Post, Error>) -> Void) {
        guard let url = URL(string: urlString) else {
            completion(
                .failure(
                    NSError(domain: "", code: -1, userInfo: [NSLocalizedDescriptionKey: "Invalid URL"])
                )
            )
            return
        }
        
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            if let error = error {
                completion(.failure(error))
                return
            }
            
            guard let data,
                  let post = try? JSONDecoder().decode(Post.self, from: data)
            else {
                completion(
                    .failure(
                        NSError(domain: "", code: -1, userInfo: [NSLocalizedDescriptionKey: "No data received"])
                    )
                )
                return
            }
            
            completion(.success(post))
        }
        
        task.resume()
    }

}
