//
//  WorkerViewController.swift
//  Practice 4
//
//  Created by Егор Бадмаев on 14.04.2024.
//

import UIKit
import BackgroundTasks

final class WorkerViewController: UIViewController {
    
    private let label: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 14, weight: .bold)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(label)
        NSLayoutConstraint.activate([
            label.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            label.centerYAnchor.constraint(equalTo: view.centerYAnchor),
        ])
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let userDefaults = UserDefaults.standard
        let date = Date(
            timeIntervalSince1970: TimeInterval(userDefaults.integer(forKey: "lastCleaned"))
        )
        let number = userDefaults.integer(forKey: "appRefreshed")
        label.text = "Last clean date: \(date)\nNumber of app refreshes: \(number)"
    }
}
