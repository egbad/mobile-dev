//
//  CompassViewController.swift
//  Practice 5
//
//  Created by Егор Бадмаев on 28.04.2024.
//

import UIKit
import CoreMotion

final class CompassViewController: UIViewController {
    
    private let locationManager = CLLocationManager()
    
    private let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(systemName: "arrowshape.up.fill")
        return imageView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageView.frame = CGRect(x: view.bounds.midX - 50, y: view.bounds.midY - 50, width: 100, height: 100)
        view.addSubview(imageView)
        view.backgroundColor = .systemBackground
        
        locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.headingAvailable() {
            locationManager.delegate = self
            locationManager.startUpdatingHeading()
        }
    }
}

extension CompassViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        let angle = CGFloat(newHeading.magneticHeading)
        
        UIView.animate(withDuration: 0.1) {
            self.imageView.transform = CGAffineTransform(rotationAngle: angle * .pi / 180)
        }
    }
}
