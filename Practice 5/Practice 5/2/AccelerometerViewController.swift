//
//  AccelerometerViewController.swift
//  Practice 5
//
//  Created by Егор Бадмаев on 28.04.2024.
//

import UIKit
import CoreMotion

final class AccelerometerViewController: UIViewController {
    
    private lazy var azimuthLabel = createLabel()
    private lazy var pitchLabel = createLabel()
    private lazy var rollLabel = createLabel()
    
    private let motion: CMMotionManager = {
        let motion = CMMotionManager()
        motion.accelerometerUpdateInterval = 0.5
        motion.showsDeviceMovementDisplay = true
        return motion
    }()
    private let queue: OperationQueue = {
        let queue = OperationQueue()
        queue.qualityOfService = .userInteractive
        return queue
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(azimuthLabel)
        view.addSubview(pitchLabel)
        view.addSubview(rollLabel)
        
        NSLayoutConstraint.activate([
            pitchLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            pitchLabel.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            
            azimuthLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            azimuthLabel.bottomAnchor.constraint(equalTo: pitchLabel.topAnchor, constant: -16),
            
            rollLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            rollLabel.topAnchor.constraint(equalTo: pitchLabel.bottomAnchor, constant: 16)
        ])
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if motion.isDeviceMotionAvailable {
            motion.startAccelerometerUpdates(to: queue) { data, error in
                if let data {
                    DispatchQueue.main.async {
                        self.azimuthLabel.text = "\(data.acceleration.x)"
                        self.pitchLabel.text = "\(data.acceleration.z)"
                        self.rollLabel.text = "\(data.acceleration.y)"
                    }
                }
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if motion.isDeviceMotionAvailable {
            motion.stopAccelerometerUpdates()
        }
    }
    
    private func createLabel() -> UILabel {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }
}
