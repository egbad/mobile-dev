//
//  ViewController.swift
//  Practice 5
//
//  Created by Егор Бадмаев on 28.04.2024.
//

import UIKit
import SensorKit

final class SensorsViewController: UIViewController {
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .insetGrouped)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()
    
    private let data: [SRSensor] = [
        SRSensor.accelerometer,
        SRSensor.ambientLightSensor,
        SRSensor.ambientPressure,
        SRSensor.deviceUsageReport,
        SRSensor.faceMetrics,
        SRSensor.heartRate,
        SRSensor.keyboardMetrics,
        SRSensor.mediaEvents,
        SRSensor.messagesUsageReport,
        SRSensor.odometer,
        SRSensor.onWristState,
        SRSensor.pedometerData,
        SRSensor.phoneUsageReport,
        SRSensor.rotationRate,
        SRSensor.siriSpeechMetrics,
        SRSensor.telephonySpeechMetrics,
        SRSensor.visits,
        SRSensor.wristTemperature
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Sensors List"
        view.backgroundColor = .secondarySystemBackground
        view.addSubview(tableView)
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor)
        ])
    }
}

extension SensorsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = data[indexPath.row].rawValue
        return cell
    }
}
