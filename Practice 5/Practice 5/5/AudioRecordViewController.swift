//
//  AudioRecordViewController.swift
//  Practice 5
//
//  Created by Егор Бадмаев on 28.04.2024.
//

import UIKit
import AVFoundation

final class AudioRecordViewController: UIViewController {
    
    private var audioRecorder: AVAudioRecorder?
    private var audioPlayer: AVAudioPlayer?
    private var isRecording: Bool = false
    
    private var documentsDirectory: URL {
        FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    }
    private var audioFileName: URL {
        documentsDirectory.appendingPathComponent("recording.m4a")
    }
    
    @IBAction func onPlayButtonAction(_ sender: Any) {
        let settings = [
            AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
            AVSampleRateKey: 12000,
            AVNumberOfChannelsKey: 1,
            AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
        ]
        let recordingSession = AVAudioSession.sharedInstance()
        
        do {
            try recordingSession.setCategory(.playAndRecord, mode: .default)
            try recordingSession.setActive(true)
        } catch {
            showAlert(with: "Не удалось сохранить запись")
        }
        
        do {
            audioRecorder = try AVAudioRecorder(url: audioFileName, settings: settings)
            audioRecorder?.delegate = self
            audioRecorder?.record()
            isRecording = true
        } catch {
            showAlert(with: "Не удалось сохранить запись")
        }
    }
    
    @IBAction func onStopButtonAction(_ sender: Any) {
        audioRecorder?.stop()
        isRecording = false
    }
    
    @IBAction func onSpeakButtonAction(_ sender: Any) {
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: audioFileName)
            audioPlayer?.delegate = self
            audioPlayer?.prepareToPlay()
            audioPlayer?.play()
        } catch {
            showAlert(with: "Ошибка воспроизведения аудиофайла")
        }
    }
    
    private func showAlert(with title: String) {
        let alert = UIAlertController(title: title, message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default))
        present(alert, animated: true, completion: nil)
    }
}

extension AudioRecordViewController: AVAudioRecorderDelegate, AVAudioPlayerDelegate {
    
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if !flag {
            showAlert(with: "Не удалось сохранить запись")
        }
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        if !flag {
            showAlert(with: "Ошибка воспроизведения аудиофайла")
        }
    }
}
