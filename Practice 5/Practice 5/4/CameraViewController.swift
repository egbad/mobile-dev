//
//  CameraViewController.swift
//  Practice 5
//
//  Created by Егор Бадмаев on 28.04.2024.
//

import UIKit
import AVFoundation

final class CameraViewController: UIViewController {
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(systemName: "camera")
        imageView.contentMode = .scaleAspectFit
        imageView.isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(imageTapped))
        imageView.addGestureRecognizer(tapGesture)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    private var documentsDirectory: URL {
        FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(imageView)
        NSLayoutConstraint.activate([
            imageView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            imageView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            imageView.widthAnchor.constraint(equalToConstant: 200),
            imageView.heightAnchor.constraint(equalToConstant: 200)
        ])
    }
    
    @objc private func imageTapped() {
        AVCaptureDevice.requestAccess(for: AVMediaType.video) { granted in
            if granted {
                DispatchQueue.main.async {
                    let imagePickerController = UIImagePickerController()
                    imagePickerController.delegate = self
                    imagePickerController.sourceType = .camera
                    self.present(imagePickerController, animated: true, completion: nil)
                }
            } else {
                DispatchQueue.main.async {
                    let alert = UIAlertController(
                        title: "Доступ к камере",
                        message: "Для работы приложения необходим доступ к камере. Пожалуйста, разрешите доступ в настройках приложения.",
                        preferredStyle: .alert
                    )
                    alert.addAction(
                        UIAlertAction(
                            title: "Открыть настройки",
                            style: .default,
                            handler: { action in
                                UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
                            }
                        )
                    )
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
    
    private func saveImageToAppDirectory(image: UIImage) {
        guard let data = image.jpegData(compressionQuality: 1.0) else { return }
        
        DispatchQueue.global(qos: .utility).async {
            let filename = self.documentsDirectory.appendingPathComponent("IMAGE_\(Date())_")
            try? data.write(to: filename)
        }
    }
}

extension CameraViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[.originalImage] as? UIImage {
            imageView.image = pickedImage
            saveImageToAppDirectory(image: pickedImage)
            picker.dismiss(animated: true, completion: nil)
        }
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}
