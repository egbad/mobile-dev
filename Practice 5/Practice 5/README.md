# Практическая №4

| Sensors 1 | Sensors 2 | Accelerometer 1 |
|---|---|---|
| ![!](Screens/Sensors1.png) | ![!](Screens/Sensors2.png) | ![!](Screens/Accelerometer1.PNG) |
| Accelerometer 2 | Accelerometer 3 | Camera 1 |
| ![!](Screens/Accelerometer2.PNG) | ![!](Screens/Accelerometer3.PNG) | ![!](Screens/Camera1.png) |
| Camera 2 | Camera 3 | AudioRecord 1 |
| ![!](Screens/Camera2.PNG) | ![!](Screens/Camera3.PNG) | ![!](Screens/AudioRecord1.png) |
| AudioRecord 2 | AudioRecord 3 | North Direction |
| ![!](Screens/AudioRecord2.PNG) | ![!](Screens/AudioRecord3.PNG) | ![!](Screens/NorthDirection.PNG) |
| Profile 1 | Profile 2 | Profile 3 |
| ![!](Screens/Profile1.png) | ![!](Screens/Profile2.jpg) | ![!](Screens/Profile3.PNG) |
| Permissions 1 | Permissions 2 | Main.storyboard 1 |
| ![!](Screens/Last.PNG) | ![!](Screens/Info.plist.png) | ![!](Screens/Main.storyboard1.png) |
| Main.storyboard 2 |   |   |
| ![!](Screens/Main.storyboard2.png) |   |   |