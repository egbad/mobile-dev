//
//  MyLooper.swift
//  Practice 4
//
//  Created by Егор Бадмаев on 14.04.2024.
//

import Foundation

class Looper {
    private var runLoop: RunLoop
    
    public var mHandler: (() -> (Int, String))?
    private var mainHandler: (String) -> Void
    
    init(mainThreadHandler: @escaping (String) -> Void) {
        runLoop = RunLoop.current
        mainHandler = mainThreadHandler
    }
    
    func run() {
        print("MyLooper run")
        runLoop.add(Port(), forMode: .default)
        runLoop.run()
        runLoop.perform { [weak self] in
            guard let self else { return }
            
            if let message = self.mHandler?() {
                self.handleMessage(job: message.1, age: message.0)
            }
        }
    }
    
    func stop() {
        runLoop.remove(Port(), forMode: .default)
        runLoop.run(until: Date())
    }
    
    private func handleMessage(job: String, age: Int) {
        print("Looper получил сообщение. Возраст: \(age), должность: \(job)")
        sleep(UInt32(age))
        mainHandler("Мой возраст - \(age) лет, моя должность - \(job)")
    }
}
