//
//  LooperViewController.swift
//  Practice 4
//
//  Created by Егор Бадмаев on 19.04.1624.
//

import UIKit
import Combine

final class LooperViewController: UIViewController {
    
    private lazy var ageTextField: UITextField = {
        let ageTextField = UITextField()
        ageTextField.placeholder = "Age"
        ageTextField.borderStyle = .roundedRect
        ageTextField.translatesAutoresizingMaskIntoConstraints = false
        return ageTextField
    }()
    
    private lazy var jobTextField: UITextField = {
        let jobTextField = UITextField()
        jobTextField.placeholder = "Job"
        jobTextField.borderStyle = .roundedRect
        jobTextField.translatesAutoresizingMaskIntoConstraints = false
        return jobTextField
    }()
    
    private lazy var submitButton: UIButton = {
        let submitButton = UIButton(type: .system)
        submitButton.setTitle("Submit", for: .normal)
        submitButton.addTarget(self, action: #selector(submitButtonTapped), for: .touchUpInside)
        submitButton.translatesAutoresizingMaskIntoConstraints = false
        return submitButton
    }()
    
    private lazy var textView: UITextView = {
        let textView = UITextView()
        textView.text = "Мой номер по списку №2"
        textView.translatesAutoresizingMaskIntoConstraints = false
        return textView
    }()
    
    private lazy var looper = Looper(mainThreadHandler: { [weak self] result in
        guard let self else { return }
        
        self.textView.text = result
    })
    
    private var cancellables = Set<AnyCancellable>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addSubviews()
        setupConstraints()
    }
    
    private func addSubviews() {
        view.addSubview(ageTextField)
        view.addSubview(jobTextField)
        view.addSubview(submitButton)
        view.addSubview(textView)
    }
    
    private func setupConstraints() {
        NSLayoutConstraint.activate([
            ageTextField.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 16),
            ageTextField.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 16),
            ageTextField.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -16),
            
            jobTextField.topAnchor.constraint(equalTo: ageTextField.bottomAnchor, constant: 16),
            jobTextField.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 16),
            jobTextField.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -16),
            
            submitButton.topAnchor.constraint(equalTo: jobTextField.bottomAnchor, constant: 16),
            submitButton.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 16),
            submitButton.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -16),
            
            textView.centerXAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerXAnchor),
            textView.centerYAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerYAnchor),
            textView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 16),
            textView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -16),
            textView.heightAnchor.constraint(equalToConstant: 280)
        ])
    }
    
    @objc private func submitButtonTapped() {
        guard let job = jobTextField.text,
              let ageText = ageTextField.text,
              let age = Int(ageText),
              age > 0
        else { return }
        
        looper.mHandler = { (age, job) }
        
        Thread {
            self.looper.run()
            DispatchQueue.main.async {
                self.looper.stop()
            }
        }.start()
    }
}
