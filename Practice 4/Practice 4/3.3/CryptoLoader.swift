//
//  CryptoLoader.swift
//  Practice 4
//
//  Created by Егор Бадмаев on 14.04.2024.
//

import CryptoKit
import Foundation

final class CryptoLoader {
    
    private let key = SymmetricKey(size: SymmetricKeySize(bitCount: 128))
    
    func encrypt(message: String) -> AES.GCM.SealedBox? {
        guard let data = message.data(using: .utf8) else {
            return nil
        }
        let encryptedData = try? AES.GCM.seal(data, using: key)
        return encryptedData
    }
    
    func decrypt(sealedBox: AES.GCM.SealedBox, completion: @escaping (String?) -> Void) {
        // Уходим в бекграунд (на фон)
        DispatchQueue.global(qos: .background).async {
            do {
                let data = try AES.GCM.open(sealedBox, using: self.key)
                completion(String(data: data, encoding: .utf8))
            } catch {
                completion(nil)
            }
        }
    }
}
