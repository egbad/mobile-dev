//
//  CryptoLoaderViewController.swift
//  Practice 4
//
//  Created by Егор Бадмаев on 14.04.2024.
//

import UIKit

final class CryptoLoaderViewController: UIViewController {
    
    private let cryptoLoader = CryptoLoader()
    
    private let textField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "Введите фразу"
        return textField
    }()
    
    private lazy var button: UIButton = {
        let button = UIButton()
        button.backgroundColor = .systemGreen
        button.layer.cornerRadius = 8
        button.setTitle("Зашифровать", for: .normal)
        button.addTarget(self, action: #selector(encryptButtonTapped), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 16
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        stackView.addArrangedSubview(textField)
        stackView.addArrangedSubview(button)
        view.addSubview(stackView)
        
        NSLayoutConstraint.activate([
            stackView.centerXAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerXAnchor),
            stackView.centerYAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerYAnchor),
            stackView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 16),
            stackView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -16)
        ])
    }
    
    @objc private func encryptButtonTapped() {
        guard let message = textField.text,
              let encryptedMessage = cryptoLoader.encrypt(message: message)
        else {
            return
        }
        
        cryptoLoader.decrypt(sealedBox: encryptedMessage) { result in
            if let result {
                DispatchQueue.main.async {
                    let alertController = UIAlertController(title: "Расшифрованное сообщение", message: result, preferredStyle: .alert)
                    let defaultAction = UIAlertAction(title: "ОК", style: .default, handler: nil)
                     alertController.addAction(defaultAction)
                    self.present(alertController, animated: true, completion: nil)
                }
            } else {
                print("Ошибка!")
            }
        }
    }
}
