//
//  ThreadsViewController.swift
//  Practice 4
//
//  Created by Егор Бадмаев on 19.04.1624.
//

import UIKit
import Combine

final class ThreadsViewController: UIViewController {
    
    private lazy var daysTextField: UITextField = {
        let daysTextField = UITextField()
        daysTextField.placeholder = "Количество дней"
        daysTextField.borderStyle = .roundedRect
        daysTextField.translatesAutoresizingMaskIntoConstraints = false
        return daysTextField
    }()
    private var lessonsTextField: UITextField = {
        let lessonsTextField = UITextField()
        lessonsTextField.placeholder = "Количество пар"
        lessonsTextField.borderStyle = .roundedRect
        lessonsTextField.translatesAutoresizingMaskIntoConstraints = false
        return lessonsTextField
    }()
    private lazy var calculateButton: UIButton = {
        let calculateButton = UIButton(type: .system)
        calculateButton.setTitle("Рассчитать", for: .normal)
        calculateButton.addTarget(self, action: #selector(calculateButtonTapped), for: .touchUpInside)
        calculateButton.translatesAutoresizingMaskIntoConstraints = false
        return calculateButton
    }()
    private var textView: UITextView = {
        let textView = UITextView()
        textView.isScrollEnabled = false
        textView.translatesAutoresizingMaskIntoConstraints = false
        return textView
    }()
    private lazy var threadNameLabel: UILabel = {
        let threadNameLabel = UILabel()
        threadNameLabel.text = "Новое имя потока: \(Thread.current.name ?? "")\nПредыдущее имя потока: \(previousName ?? "")"
        threadNameLabel.numberOfLines = 0
        threadNameLabel.translatesAutoresizingMaskIntoConstraints = false
        return threadNameLabel
    }()
    
    private var counter: Int = 0
    private var previousName: String?
    
    @Published private var output: String = ""
    private var cancellables = Set<AnyCancellable>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        previousName = Thread.current.name
        Thread.current.name = "МОЙ НОМЕР ГРУППЫ: 10, НОМЕР ПО СПИСКУ: 2, МОЙ ЛЮБИМЫЙ ФИЛЬМ: Назад в будущее"
        print("Stack: \(Thread.callStackSymbols)")
        
        setupViews()
        addConstraints()
        setupBindings()
    }
    
    private func setupBindings() {
        $output
            .sink { [weak self] newText in
                self?.textView.text = newText
            }
            .store(in: &cancellables)
    }
    
    private func setupViews() {
        view.addSubview(daysTextField)
        view.addSubview(lessonsTextField)
        view.addSubview(calculateButton)
        view.addSubview(textView)
        view.addSubview(threadNameLabel)
    }
    
    private func addConstraints() {
        NSLayoutConstraint.activate([
            daysTextField.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 16),
            daysTextField.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 16),
            daysTextField.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -16),
            
            lessonsTextField.topAnchor.constraint(equalTo: daysTextField.bottomAnchor, constant: 16),
            lessonsTextField.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 16),
            lessonsTextField.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -16),
            
            calculateButton.topAnchor.constraint(equalTo: lessonsTextField.bottomAnchor, constant: 16),
            calculateButton.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 16),
            calculateButton.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -16),
            
            textView.topAnchor.constraint(equalTo: calculateButton.bottomAnchor, constant: 16),
            textView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 16),
            textView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -16),
            textView.heightAnchor.constraint(equalToConstant: 40),
            
            threadNameLabel.topAnchor.constraint(equalTo: textView.bottomAnchor, constant: 16),
            threadNameLabel.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 16),
            threadNameLabel.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -16)
        ])
    }
    
    @objc private func calculateButtonTapped() {
        Thread {
            let threadNumber = self.counter
            self.counter += 1
            print("Запущен поток №\(threadNumber) студентом группы БСБО-10-21 номер по списку №17")
            
            let endTime = Date().addingTimeInterval(20)
            while Date() < endTime {
                Thread.sleep(forTimeInterval: 0.1)
            }
            print("Выполнен поток №\(threadNumber)")
        }.start()
        
        guard let lessons = Double(lessonsTextField.text ?? ""),
              let days = Double(daysTextField.text ?? ""),
              days > 0.0
        else {
            output = "Введены некорректные данные"
            return
        }
        
        Thread {
            let result = String(lessons / days)
            DispatchQueue.main.async {
                self.output = "Среднее кол-во пар: " + result
            }
        }.start()
    }
}
