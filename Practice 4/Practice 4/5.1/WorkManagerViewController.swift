//
//  WorkManagerViewController.swift
//  Practice 4
//
//  Created by Егор Бадмаев on 14.04.2024.
//

import UIKit
import SystemConfiguration

final class WorkManagerViewController: UIViewController {
    
    private let uploadWorker = UploadWorker()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        uploadWorker.addConstraint {
            var zeroAddress = sockaddr_in()
            zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
            zeroAddress.sin_family = sa_family_t(AF_INET)
            
            let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
                $0.withMemoryRebound(to: sockaddr.self, capacity: 1) { zeroSockAddress in
                    SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
                }
            }
            
            var flags: SCNetworkReachabilityFlags = []
            if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
                return false
            }
            let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
            let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
            return (isReachable && !needsConnection)
        }
        uploadWorker.addConstraint {
            let device = UIDevice.current
            device.isBatteryMonitoringEnabled = true
            return device.batteryLevel > 0.5
        }
        
        let operationQueue = OperationQueue()
        operationQueue.addOperation(uploadWorker)
    }
}
