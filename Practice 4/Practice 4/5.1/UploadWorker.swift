//
//  UploadWorker.swift
//  Practice 4
//
//  Created by Егор Бадмаев on 14.04.2024.
//

import Foundation

class UploadWorker: Operation {
    
    static let tag = "UploadWorker"
    
    private var constraints: [() -> Bool] = []
    
    override func main() {
        let result = constraints.allSatisfy { constraint in
            constraint()
        }
        
        if result {
            print("\(UploadWorker.tag): start")
            sleep(5)
            print("\(UploadWorker.tag): end")
        } else {
            print("\(UploadWorker.tag): constraints were not satisfied")
        }
    }
    
    func addConstraint(_ constraint: @escaping () -> Bool) {
        constraints.append(constraint)
    }
}
