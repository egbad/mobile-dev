//
//  ViewController.swift
//  Practice 4
//
//  Created by Егор Бадмаев on 14.04.2024.
//

import UIKit
import Combine

final class ViewController: UIViewController {
    
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var stopButton: UIButton!
    @IBOutlet weak var timeSlider: UISlider!
    @IBOutlet weak var currentTimeLabel: UILabel!
    @IBOutlet weak var endTimeLabel: UILabel!
    
    @IBOutlet weak var sliderWrapper: UIStackView!
    
    private var playerService = PlayerService()
    private var cancellables = Set<AnyCancellable>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        [playButton, stopButton].forEach {
            $0.layer.cornerRadius = $0.frame.size.height / 2
        }
        
        setupBindings()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        sliderWrapper.isHidden = UIDevice.current.orientation.isLandscape
    }
    
    private func setupBindings() {
        playButton.addTarget(self, action: #selector(playButtonAction), for: .touchUpInside)
        stopButton.addTarget(self, action: #selector(stopButtonAction), for: .touchUpInside)
        timeSlider.addTarget(self, action: #selector(timeSliderChanged), for: .valueChanged)
        
        playerService.$currentTime
            .sink { [weak self] currentTime in
                self?.timeSlider.value = Float(currentTime)
                self?.currentTimeLabel.text = self?.timeString(currentTime)
            }
            .store(in: &cancellables)
        
        playerService.$duration
            .sink { [weak self] duration in
                self?.timeSlider.maximumValue = Float(duration)
                self?.endTimeLabel.text = self?.timeString(duration)
            }
            .store(in: &cancellables)
    }
    
    private func timeString(_ time: TimeInterval) -> String {
        let minute = Int(time) / 60
        let seconds = Int(time) % 60
        return String(format: "%02d:%02d", minute, seconds)
    }
    
    @objc private func playButtonAction() {
        playerService.play()
        playButton.isEnabled = false
        stopButton.isEnabled = true
    }
    
    @objc private func stopButtonAction() {
        playerService.pause()
        playButton.isEnabled = true
        stopButton.isEnabled = false
    }
    
    @objc private func timeSliderChanged() {
        playerService.setCurrentTime(to: TimeInterval(timeSlider.value))
    }
}
