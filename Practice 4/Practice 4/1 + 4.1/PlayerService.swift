//
//  PlayerService.swift
//  Practice 4
//
//  Created by Егор Бадмаев on 14.04.2024.
//

import Combine
import AVFoundation
import UserNotifications

final class PlayerService: NSObject, ObservableObject, AVAudioPlayerDelegate {
    
    @Published var duration: TimeInterval = 0.0
    @Published var currentTime: TimeInterval = 0.0
    @Published var isPlaying: Bool = false
    
    private var audioPlayer: AVAudioPlayer?
    private var timer: Timer?
    
    override init() {
        super.init()
        setupNotifications()
        setupAudioPlayer()
    }
    
    deinit {
        if timer != nil {
            timer = nil
        }
    }
    
    func play() {
        audioPlayer?.play()
        isPlaying = true
        timer = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true) { [weak self] _ in
            self?.currentTime = self?.audioPlayer?.currentTime ?? 0.0
        }
        sendNotification()
    }
    
    func pause() {
        audioPlayer?.pause()
        isPlaying = false
        timer = nil
    }
    
    func setCurrentTime(to timeInterval: TimeInterval) {
        audioPlayer?.currentTime = timeInterval
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        guard flag else {
            assertionFailure("Не удалось успешно завершить воспроизведение песни")
            return
        }
        
        pause()
        UNUserNotificationCenter.current().removeAllDeliveredNotifications()
    }
    
    private func setupAudioPlayer() {
        guard let url = Bundle.main.url(forResource: "Havana", withExtension: "mp3") else {
            assertionFailure("Нет ресурса песни")
            return
        }
        
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: url)
            audioPlayer?.prepareToPlay()
            audioPlayer?.delegate = self
            duration = audioPlayer?.duration ?? 0.0
        } catch {
            assertionFailure("Ошибка инициализации плеера: \(error)")
        }
    }
    
    private func setupNotifications() {
        UNUserNotificationCenter.current().requestAuthorization(
            options: [.alert, .badge, .sound]
        ) { success, error in
            if success {
                print("Разрешение на уведомления получено")
            } else if let error {
                print("Ошибка при запросе разрешения: \(error.localizedDescription)")
            }
        }
    }
    
    private func sendNotification() {
        let content = UNMutableNotificationContent()
        content.title = "Бадмаев Е.А. - song Havana"
        content.body = "MIREA Notification"
        content.sound = UNNotificationSound.default
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
        let request = UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().add(request) { error in
            if let error = error {
                print("Ошибка при добавлении уведомления: \(error.localizedDescription)")
            }
        }
    }
}
