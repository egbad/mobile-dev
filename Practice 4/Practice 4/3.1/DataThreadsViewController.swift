//
//  DataThreadsViewController.swift
//  Practice 4
//
//  Created by Егор Бадмаев on 14.04.2024.
//

import UIKit

final class DataThreadsViewController: UIViewController {
    
    private lazy var textView: UITextView = {
        let textView = UITextView()
        textView.textContainer.maximumNumberOfLines = 10
        textView.translatesAutoresizingMaskIntoConstraints = false
        return textView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(textView)
        NSLayoutConstraint.activate([
            textView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            textView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            textView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16),
            textView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16),
            textView.heightAnchor.constraint(equalToConstant: 280)
        ])
        
        DispatchQueue.main.async {
            self.textView.text = "runOnUiThread == DispatchQueue.main.async\n"
            self.textView.text.append("Выполняется немедленно в основном потоке.\n")
            self.textView.text.append("---\n")
        }
        
        perform(#selector(waitingUntilDoneAction), on: Thread.main, with: nil, waitUntilDone: true)
        
        perform(#selector(afterDelayAction), with: nil, afterDelay: 5.0)
    }
    
    @objc func waitingUntilDoneAction() {
        DispatchQueue.main.async {
            self.textView.text.append("post == UIView.performSelector\n")
            self.textView.text.append("Выполняется в основном потоке после завершения всех предыдущих операций.\n")
            self.textView.text.append("---\n")
        }
    }
    
    @objc func afterDelayAction() {
        DispatchQueue.main.async {
            self.textView.text.append("postDelayed == UIView.performSelector(afterDelay: TimeInterval)\n")
            self.textView.text.append("Выполняется в основном потоке после задержки.\n")
            self.textView.text.append("---\n")
        }
    }
}
