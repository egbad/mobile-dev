//
//  AppDelegate.swift
//  Practice 4
//
//  Created by Егор Бадмаев on 14.04.2024.
//

import UIKit
import BackgroundTasks

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        BGTaskScheduler.shared.register(forTaskWithIdentifier: "bgTask1", using: nil) { task in
            self.handleAppRefresh(task: task as! BGAppRefreshTask)
        }
    
        BGTaskScheduler.shared.register(forTaskWithIdentifier: "bgTask2", using: nil) { task in
            self.handleDatabaseCleaning(task: task as! BGProcessingTask)
        }
        return true
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        scheduleAppRefresh()
        scheduleDatabaseCleaningIfNeeded()
    }
    
    func scheduleAppRefresh() {
        let request = BGAppRefreshTaskRequest(identifier: "bgTask1")
        request.earliestBeginDate = Date(timeIntervalSinceNow: 5)
        
        do {
            try BGTaskScheduler.shared.submit(request)
        } catch {
            print("Could not schedule app refresh: \(error)")
        }
    }
    
    func scheduleDatabaseCleaningIfNeeded() {
        let lastCleaned = UserDefaults.standard.integer(forKey: "lastCleaned")
        let lastCleanDate = Date(timeIntervalSince1970: TimeInterval(lastCleaned))
        
        let now = Date()
        let oneDay = TimeInterval(24 * 60 * 60)
        
        guard now > (lastCleanDate + oneDay) else { return }
        
        let request = BGProcessingTaskRequest(identifier: "bgTask2")
        request.requiresNetworkConnectivity = false
        request.requiresExternalPower = true
        
        do {
            try BGTaskScheduler.shared.submit(request)
        } catch {
            print("Could not schedule database cleaning: \(error)")
        }
    }
    
    func handleAppRefresh(task: BGAppRefreshTask) {
        Task {
            print("Какая-то операция по обновлению приложения")
            let oldValue = UserDefaults.standard.integer(forKey: "appRefreshed")
            UserDefaults.standard.set(oldValue + 1, forKey: "appRefreshed")
            task.setTaskCompleted(success: true)
            scheduleAppRefresh()
        }
    }
    
    func handleDatabaseCleaning(task: BGProcessingTask) {
        Task {
            print("Очищаем БД и сбрасываем дату последнего обновления")
            UserDefaults.standard.set(Date().timeIntervalSince1970, forKey: "lastCleaned")
            UserDefaults.standard.set(nil, forKey: "appRefreshed")
            task.setTaskCompleted(success: true)
        }
    }
    
    // MARK: UISceneSession Lifecycle
    
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
}
