# Практическая №4

| Music Player 1 | Music Player Notification | Music Player 3 |
|---|---|---|
| ![!](Screens/mp1.png) | ![!](Screens/mp2.png) | ![!](Screens/mp3.png) |
| Music Player 4 | Music Player Deleted Notifications | Threads 1 |
| ![!](Screens/mp4.png) | ![!](Screens/mp5.png) | ![!](Screens/threads.png) |
| Threads 2 | Looper 1 | Looper 2 |
| ![!](Screens/threads2.png) | ![!](Screens/looper.png) | ![!](Screens/looper2.png) |
| Looper 3 | Data Thread 1 | Data Thread 2 |
| ![!](Screens/looper3.png) | ![!](Screens/data_thread.png) | ![!](Screens/data_thread2.png) |
| Crypto Loader 1 | Crypto Loader 2 | Upload Worker 1 |
| ![!](Screens/crypto_loader.png) | ![!](Screens/crypto_loader2.png) | ![!](Screens/upload_worker.png) |
| Upload Worker 2 | Worker 1 | Worker 2 |
| ![!](Screens/upload_worker2.png) | ![!](Screens/worker.PNG) | ![!](Screens/worker2.PNG) |
